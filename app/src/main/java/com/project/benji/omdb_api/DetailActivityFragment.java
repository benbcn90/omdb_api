package com.project.benji.omdb_api;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.project.benji.omdb_api.databinding.FragmentDetailBinding;

public class DetailActivityFragment extends Fragment {
    private View view;

    private FragmentDetailBinding binding;

    public DetailActivityFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        view = inflater.inflate(R.layout.fragment_detail, container, false);

        binding = FragmentDetailBinding.inflate(inflater);
        view = binding.getRoot();

        Intent i = getActivity().getIntent();

        if(i != null){
            OMDbApi omDbApi =new  OMDbApi();
            Movie movie = (Movie) i.getSerializableExtra("movie");
//            movie =  omDbApi.getMoviesImdbID(movie.getImdbID()) != null &&  !omDbApi.getMoviesImdbID(movie.getImdbID()).isEmpty() ? omDbApi.getMoviesImdbID(movie.getImdbID()).get(0) : new Movie();
//            movie = omDbApi.getMoviesTitleImdbID(movie.getTitle(), movie.getImdbID()).get(0);

            if(movie != null){
                updateUi(movie);
            }
        }

        SharedViewModel sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);
        sharedViewModel.getSelected().observe(this, new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                updateUi(movie);
            }
        });

        return view;
    }

    private void updateUi(Movie movie) {

        binding.tvTitle.setText(movie.getTitle());
        binding.tvType.setText(movie.getType());
        binding.tvActors.setText(movie.getActors());
        binding.tvDirector.setText(movie.getDirector());
        binding.tvGender.setText(movie.getGender());
        binding.tvWriter.setText(movie.getWriter());
        binding.tvPlot.setText(movie.getPlot());

        Glide
                .with(getContext())
                .load(movie.getPoster())
                .into(binding.ivMovieImage);
    }

}
