package com.project.benji.omdb_api;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class OMDbApi {
    //https://gateway.marvel.com/v1/public/characters?name=Spider-Man&ts=1&apikey=c421a15477a4ceaed06e11881cb4672f&hash=841fa17cb68f4541e5b0cdef5d764f5e
//    http://www.omdbapi.com/?s=Avenger&apikey=74c3c061
    private final String BASE_URL = "http://www.omdbapi.com/?";
    private final String API_KEY = "74c3c061";
    private final String SEARCH_DEFAULT = "Avenger";
    private final int PAGES = 5;
    private boolean isImdbID;

    ArrayList<Movie> getMovies() {
        this.isImdbID = false;

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("s", SEARCH_DEFAULT)
                .appendQueryParameter("apikey", API_KEY)
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }
    ArrayList<Movie> getMoviesTitleImdbID(String title, String imdbID) {
        //Informo que el tipo de busqueda es por el id para que devuelva más contenido
        this.isImdbID = true;

        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("t",title)
                .appendQueryParameter("i",imdbID)
                .appendQueryParameter("apikey", API_KEY)
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    ArrayList<Movie> getMoviesTitleType(String title, String type) {
        Uri builtUri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendQueryParameter("s",title)
                .appendQueryParameter("type", type)
                .appendQueryParameter("apikey", API_KEY)
                .build();
        String url = builtUri.toString();

        return doCall(url);
    }

    private ArrayList<Movie> doCall(String url) {
        ArrayList<Movie> cards = new ArrayList<>();

        for(int i = 0; i < PAGES; i++){
            try {
                String JsonResponse = HttpUtils.get(url);
                return processJson(JsonResponse);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return cards;
    }

    private ArrayList<Movie> processJson(String jsonResponse) {
        ArrayList<Movie> cards = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);

            if(!this.isImdbID) {
                JSONArray jsonMovies = data.getJSONArray("Search");
                for (int i = 0; i < jsonMovies.length(); i++) {
                    JSONObject jsonMovie = jsonMovies.getJSONObject(i);

                    Movie movie = new Movie();
                    movie.setTitle(jsonMovie.getString("Title"));
                    movie.setYear(jsonMovie.getString("Year"));
                    movie.setType(jsonMovie.getString("Type"));
                    movie.setImdbID(jsonMovie.getString("imdbID"));
                    movie.setPoster(jsonMovie.getString("Poster"));

                    cards.add(movie);
                }
            }else{
                //Si se busca por el id devuelve mas contenido
                Movie movie = new Movie();
                movie.setTitle(data.getString("Title"));
                movie.setYear(data.getString("Year"));
                movie.setType(data.getString("Type"));
                movie.setImdbID(data.getString("imdbID"));
                movie.setPoster(data.getString("Poster"));
                movie.setActors(data.getString("Actors"));
                movie.setDirector(data.getString("Director"));
                movie.setGender(data.getString("Genre"));
                movie.setWriter(data.getString("Writer"));
                movie.setPlot(data.getString("Plot"));

                cards.add(movie);
            }

            Log.e(null, String.valueOf(cards));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cards;
    }
}
