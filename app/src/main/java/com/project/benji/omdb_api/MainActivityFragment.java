package com.project.benji.omdb_api;

import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class MainActivityFragment extends Fragment {

    //Variables no generadas
    private View view;
    private ArrayList<Movie> items;
    private MovieAdapter adapter;
    private SharedPreferences preferences;
    private MoviesViewModel model;
    private ProgressDialog dialog;


    public MainActivityFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_main, container, false);

        //Prueba de cambio del color del fondo del fragment
//        view.setBackgroundColor(Color.BLUE);

        ListView lvMovies = (ListView) view.findViewById(R.id.lvMovies);

        items = new ArrayList<>();

        adapter = new MovieAdapter(getContext(), R.layout.lv_movie_row, items);

        lvMovies.setAdapter(adapter);

        final SharedViewModel sharedViewModel = ViewModelProviders.of(getActivity()).get(SharedViewModel.class);

        lvMovies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Movie movie = (Movie) adapterView.getItemAtPosition(i);

                if(!esTablet()){
                    Intent intent = new Intent(getContext(), DetailActivity.class);
                    intent.putExtra("movie", movie);
                    startActivity(intent);
                }else{
                    sharedViewModel.select(movie);
                }

            }
        });

        model = ViewModelProviders.of(this).get(MoviesViewModel.class);
        model.getCards().observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                adapter.clear();
                adapter.addAll(movies);
            }
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loanding...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if(mostrat){
                    dialog.show();
                }else{
                    dialog.dismiss();
                }
            }
        });

        return view;
    }

    private boolean esTablet() {
        return getResources().getBoolean(R.bool.tablet);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_cards_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            refresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        refresh();
    }

    private void refresh() {
        model.reload();
    }

}
