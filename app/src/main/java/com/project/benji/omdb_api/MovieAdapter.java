package com.project.benji.omdb_api;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

public class MovieAdapter extends ArrayAdapter<Movie> {
    public MovieAdapter(Context context, int resource, List<Movie> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie movie = getItem(position);
        Log.v("Testing card Adapter", movie.toString());

//        import com.project.benji.omdb_api.databinding.LvPelisRowBinding;
//        LvPelisRowBinding binding = null;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.lv_movie_row, parent, false);
        }

        TextView tvTitle = convertView.findViewById(R.id.tvTitle);
        TextView tvYear = convertView.findViewById(R.id.tvYear);
        TextView tvType = convertView.findViewById(R.id.tvType);
        ImageView ivMovieImage = convertView.findViewById(R.id.ivMovieImage);

        tvTitle.setText(movie.getTitle());
        tvType.setText(movie.getType());
        tvYear.setText(movie.getYear());
        Glide
                .with(getContext())
                .load(movie.getPoster())
                .into(ivMovieImage);

        return convertView;
    }
}
