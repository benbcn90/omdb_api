package com.project.benji.omdb_api;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MoviesViewModel extends AndroidViewModel {
    private final Application app;
    //private MutableLiveData<List<Card>> cards;
    private  final AppDatabase appDatabase;
    private final MovieDao movieDao;
    private static final int PAGES = 10;
    private MutableLiveData<Boolean> loading;

    public MoviesViewModel(Application application) {
        super(application);

        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(this.getApplication());
        this.movieDao = appDatabase.getMovieDao();
    }

    public LiveData<List<Movie>> getCards() {
        return movieDao.getMovies();
    }

    public void reload() {
        // do async operation to fetch users
        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading(){
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Movie>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Movie> doInBackground(Void... voids) {
            ArrayList<Movie> movies = new ArrayList<>();
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(app.getApplicationContext());
            OMDbApi api = new OMDbApi();


            String title = preferences.getString("title_search", "Avenger");
            String consultType = preferences.getString("type_list_search", "movie");

            ArrayList<Movie> result = api.getMoviesTitleType(title, consultType);

            for(Movie movie : result){
//                ArrayList<Movie> movie1Lista = api.getMoviesTitleImdbID(movie.getTitle(), movie.getImdbID());
//                movies.add(movie1);
                movies.add(api.getMoviesTitleImdbID(movie.getTitle(), movie.getImdbID()).get(0));
            }
            result.clear();
            result.addAll(movies);

//            ArrayList<Movie> result = api.getMovies();

//            Snackbar.make(view, "Refreshed!!", Snackbar.LENGTH_LONG).show();

            Log.d("DEBUG", result.toString());

            movieDao.deleteMovie();
            movieDao.addMovie(result);

            return result;
        }

        @Override
        protected void onPostExecute (ArrayList<Movie> cards){
            //El postValue no es accesible --> Commit 30ff5dcf
            //cards.postValue(results);
            super.onPostExecute(cards);
            loading.setValue(false);
        }
    }
}
